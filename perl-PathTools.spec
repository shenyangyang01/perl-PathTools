Name:           perl-PathTools
Version:        3.75
Release:        3
Summary:        PathTools Perl Module (Cwd, File::Spec)
License:        (GPL+ Artistic) and BSD
URL:            https://metacpan.org/release/PathTools
Source0:        https://cpan.metacpan.org/authors/id/X/XS/XSAWYERX/PathTools-%{version}.tar.gz
Patch0:         PathTools-3.74-Disable-VMS-tests.patch

BuildRequires:  git gcc
BuildRequires:  perl-generators
BuildRequires:  perl-interpreter
BuildRequires:  perl(ExtUtils::MakeMaker) >= 6.76
BuildRequires:  perl(Test::More) >= 0.88
Requires:       perl(:MODULE_COMPAT_%(eval "`perl -V:version`"; echo $version))
Requires:       perl(Carp)
Requires:       perl(File::Basename)
Requires:       perl(Errno)
Requires:       perl(Scalar::Util)


%{?perl_default_filter}
%description
Tools for working with directory and file names.

%package_help

%prep
%autosetup -n PathTools-%{version} -p1 -S git
#Disable VMS tests
rm -f lib/File/Spec/VMS.pm
sed -i -e '/VMS.pm/d' MANIFEST

%build
perl Makefile.PL INSTALLDIRS=vendor
%make_build

%install
rm -rf $RPM_BUILD_ROOT
make pure_install DESTDIR=$RPM_BUILD_ROOT
find $RPM_BUILD_ROOT -type f -name .packlist -delete
%{_fixperms} -c $RPM_BUILD_ROOT

%check
make test

%files
%defattr(-,root,root)
%doc Changes
%{perl_vendorarch}/Cwd.pm
%{perl_vendorarch}/File/*
%{perl_vendorarch}/auto/*

%files help
%{_mandir}/man3/*

%changelog
* Thu Sep 26 2019 shenyangyang<shenyangyang4@huawei.com> - 3.75-3
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:revise help package

* Tue Aug 13 2019 openEuler Buildteam <buildteam@openeuler.org> - 3.75-2
- Package init
